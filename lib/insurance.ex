defmodule Insurance do

  def categorias(weight) do
    if (Float.parse(weight["bmi"])<{18.5,""}) do
      {"Bajo Peso", weight}
    end
    if (Float.parse(weight["bmi"])>={18.5,""} and Float.parse(weight["bmi"])<{25,""})do
    {"Normal", weight}
    end
    if (Float.parse(weight["bmi"])>={25,""} and Float.parse(weight["bmi"])<{30,""}) do
    {"Sobrepeso", weight}
    end
    if (Float.parse(weight["bmi"])>={30,""})do
    {"Obeso", weight}
    end
    end


  def count_by_bmi_c do
    File.stream!("insurance.csv")
    |> CSV.decode!(headers: true)
    |> Enum.sort_by(fn peso-> peso["bmi"] end)
    |>Enum.map(fn weight-> categorias(weight) end)
    |>Enum.group_by(fn {agrupar, weight} -> agrupar end)

  end

  def fumadores(info)do
    x = length(Enum.filter(datos, fn info ->(info["smoker"] == "yes")end))
  end

  def relation_smoker_children do
    File.stream!("insurance.csv")
    |> CSV.decode!(heaters: true)
    |> Enum.group_by(fn info-> info["children"] end)
    |> Enum.map(fn {n, datos}->{n, fumadores(datos)}end )

  end
end
